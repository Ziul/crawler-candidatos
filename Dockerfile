FROM python:3.6-slim-jessie

ADD requirements.txt /opt/crawler/requirements.txt
ADD ./ordinarias/ /opt/crawler

WORKDIR /opt/crawler

# RUN apt-get update && apt-get install -y \
#     git \
#     gcc \
#     && rm -rf /var/lib/apt/lists/*

# install dependencies
RUN pip install -r requirements.txt

CMD  ["scrapy", "crawl", "candidatos"]
