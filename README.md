# Relação de candidatos

### Estrutura de diretórios

```
├── README.md
├── Check-Missing.ipynb
├── LoadFiles.ipynb
├── DADOS AUTORIDADES
│   ├── DADOS AUTORIDADES
│   │   ├── Entidades de Classe, Conselhos Profissionais e Câmaras de Comércio
│   │   ├── Gov Estadual
│   │   ├── Organismos Internacionais
│   │   ├── Poder Executivo
│   │   │   ├── Veículos de Comunicação
│   │   ├── Poder Judiciário
│   │   ├── Poder Legislativo
│   │   └── Veículos de Comunicação
├── ordinarias
│   ├── ordinarias
│   │   ├── __init__.py
│   │   ├── items.py
│   │   ├── middlewares.py
│   │   ├── pipelines.py
│   │   ├── settings.py
│   │   └── spiders
│   │       ├── candidatos.py
│   │       ├── __init__.py
│   └── scrapy.cfg
├── Dockerfile
└── requirements.txt
```

##### Check-Missing.ipynb

Script responsável por identificar municipios problemáticos

##### LoadFiles.ipynb

Script responsável por importar os dados dos arquivos CSV para o banco de dados

##### DADOS AUTORIDADES

Pasta raiz contendo os arquivos CSV

##### ordinarias

Pasta raiz do crawler

##### Dockerfile

Quer rodar o crawler em um container Docker? Este arquivo é o responsável por gerar um container pronto para a execução do crawler.

##### requirements.txt

Lista de pacotes de dependências do crawler

### Crawler

O crawler foi desenvolvido com o framework [Scrapy](https://docs.scrapy.org). Toda e qualquer deficiencia desta documentação referente ao crawler pode ser sanada com a documentação oficial do framework.

No arquivo [settings.py](ordinarias/ordinarias/settings.py) é possivel configurar o banco para ser utilizado o crawler e os pipelines padrões. É possivel também alterar o endereço do banco sobrescrevendo as seguintes variaveis de ambiente:

- **MONGODB_DB**: Nome da Database a ser utilizada
- **MONGODB_SERVER**: URL/URI do banco de dados
- **MONGODB_PORT**: Porta do banco de dados

Por padrão, o crawler irá jogar os dados coletados em uma coleção com o mesmo nome do *ano* da eleição que esta sendo crawleada. Para alterar o ano de interesse, basta alterar a variável *year* na [aranha](ordinarias/ordinarias/spiders/candidatos.py). Também é possivel alterar o valor apenas em tempo de execução, passando o parametro *-a*.

#### Executando o crawler

Dentro da pasta [ordinarias](ordinarias), onde se encontra o arquivo [scrapy.cfg](ordinarias/scrapy.cfg) execute o seguinte comando:

> scrapy crawl candidatos

### Recriando banco e views

- O crawler deverá criar coleções nomeadas com o ano de cada eleição. No presente caso temos as coleções para os anos de 2014, 2016 e 2018.
- Crie uma nova coleção chamada *candidatos* e salve o conteudo das coleções geradas pelo crawler nela.
- Descompacte o arquivo *zip* contendo os arquivos csv na pasta raiz, como indicado na sessão **Estrutura de diretórios**.
- Execute o script [LoadFiles.ipynb](LoadFiles.ipynb). Ele deverá criar a coleção *csv*

Com isso já teremos as duas coleções de interesse: *csv* e *candidatos*. Vamos [criar algumas *views*](https://docs.mongodb.com/manual/reference/method/db.createView/) de acordo com algumas agregações

#### missing_csv

Esta *view* tem como objetivo listar os documentos que não conseguiram ser conciliados com os dados do TSE por algum motivo na alteração dos nomes (a maioria dos governadores, por exemplo) ou simplesmente por não terem sido crawleados, como desembargadores ou chefes de gabinete. A agregação é bem simples, já  que é basicamente um filtro:

```
[{$match: {
  tse_id: null
}}]
```

#### problens_candidatos

Esta *view* tem comoobjetivo listar candidatos que tinham na API ainda situações desatualizadas (como status de *concorrendo* ou *2º turno*). É uma agregação bem simples também, tendo o cuidado apenas de filtrar quais campos seriam projetados, para facilitar a leitura:

```
[{$match: {
  descricaoTotalizacao: {
    $in: [
      'Concorrendo',
      '2º turno'
    ]
  }
}}, {$project: {
  nomeCompleto: 1,
  cargo: '$cargo.nome',
  localCandidatura: 1,
  ufSuperiorCandidatura: 1,
  descricaoSituacao: 1,
  descricaoTotalizacao: 1
}}]
```

#### problens_csv

Esta *view* é bem mais pesada e tem como objetivo listar todos os documentos da coleção **candidatos* que não conseguiram ser relacionados com algum documento na coleção *csv*. Devido a grande quantidade de documentos analisados por essa view, ela costuma ter certa dificuldade para ser renderizada.

```
[{$match: {
  tse_id: {
    $gt: 0
  }
}}, {$group: {
  _id: null,
  ids: {
    $addToSet: '$tse_id'
  }
}}, {$lookup: {
  from: 'candidatos',
  'let': {
    ids: '$ids'
  },
  pipeline: [
    {
      $match: {
        $expr: {
          $not: {
            $in: [
              '$id',
              '$$ids'
            ]
          }
        }
      }
    },
    {
      $project: {
        id: 1,
        nomeUrna: 1,
        cargo: '$cargo.nome'
      }
    }
  ],
  as: 'string'
}}, {$unwind: {
  path: '$string'
}}, {$replaceRoot: {
  newRoot: '$string'
}}]
```

#### people

Esta *view* tem como objetivo ser a penultima view. A ultima view seria projetar a coleção *missing_csv* nela, apresentando assim todos os possiveis documentos. Porém, da forma como esta hoje o banco, há ainda muita incoerencia com alguns dados, de forma a gerar personas duplicadas. Essa view tem de pesado apenas o trabalho de projeção dos campos:

```
[{$lookup: {
  from: 'csv',
  localField: 'id',
  foreignField: 'tse_id',
  as: 'csv'
}}, {$project: {
  id: 1,
  nome: {
    nomeCompleto: '$nomeCompleto',
    nomeUrna: '$nomeUrna',
    nomeCsv: {
      $arrayElemAt: [
        '$csv.Nome',
        0
      ]
    }
  },
  cargo: '$cargo.nome',
  dataDeNascimento: {
    $dateFromString: {
      dateString: '$dataDeNascimento',
      format: '%Y-%m-%d'
    }
  },
  contato: {
    emails: {
      $setUnion: [
        '$emails',
        '$csv.des_email'
      ]
    },
    telefone: {
      $split: [
        {
          $reduce: {
            input: '$csv.Telefone',
            initialValue: '',
            'in': {
              $concat: [
                '$$value',
                ',',
                '$$this'
              ]
            }
          }
        },
        ','
      ]
    },
    fax: {
      $setUnion: [
        '$csv.Fax',
        []
      ]
    }
  },
  cpf: 1,
  fotoUrl: 1,
  grauInstrucao: 1,
  localCandidatura: 1,
  numero: 1,
  partido: 1,
  sites: 1,
  ufCandidatura: 1,
  ufSuperiorCandidatura: 1,
  poder: {
    $arrayElemAt: [
      '$csv.poder',
      0
    ]
  }
}}]
```

Recomendo que, quando os dados estiverem prontos para serem mergidos, acrescente ao *pipeline* o passo de *$out*, no intuito de gerar uma nova coleção enão apenas uma view. Isso deve otimizar bastante a leitura dos dados. Caso estejam trabalhando com um mongo 4.2+, uma [view materializada](https://imasters.com.br/banco-de-dados/views-materializadas-no-mongodb-sim-e-possivel) pode ser uma solução melhor até que os dados estejam prontos.