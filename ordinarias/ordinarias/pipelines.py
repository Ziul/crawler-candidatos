# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
import logging
from ordinarias.settings import MONGO_DB as settings
from scrapy.exporters import JsonItemExporter

class JsonExporterPipleline(object):
    def open_spider(self, spider):
        self.file = open('{}.json'.format(spider.name), 'wb')
        self.exporter = JsonItemExporter(self.file, encoding="utf-8", ensure_ascii=False)
        self.exporter.start_exporting()

    def close_spider(self, spider):
        self.exporter.finish_exporting()
        self.file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item


class MongoDBPipeline(object):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings.get('MONGODB_SERVER', 'localhost'),
            settings.get('MONGODB_PORT', 27017),
            # serverSelectionTimeoutMS=10
            socketTimeoutMS=500
        )
        self.logger = logging.getLogger()
        self.db = connection[settings.get('MONGODB_DB', 'candidates')]

    def open_spider(self, spider):
        self.coll = self.db[str(spider.year)]
        # makes index sure
        if spider.name == 'candidatos':
            self.coll.create_index([('id', pymongo.ASCENDING)], unique=True, background=True)
        elif spider.name == 'wikipedia':
            self.coll.create_index([('nomeCompleto', pymongo.ASCENDING), ('cargo', pymongo.ASCENDING)],
            unique=True, background=True)
        self.logger.info('Connection with %s opened', settings.get('MONGODB_SERVER', 'localhost'))

    def process_item(self, item, spider):
        if spider.name == 'candidatos':
            self.coll.update_one({'id': item['id']}, {"$set":dict(item)}, upsert=True)
        elif spider.name == 'wikipedia':
            self.coll.update_one(
                {'nomeCompleto': item['nomeCompleto'], 'cargo': item['cargo']},
                {"$set":dict(item)}, upsert=True)

        return item