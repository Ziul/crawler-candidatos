# -*- coding: utf-8 -*-
import scrapy
import requests
import json
import pandas as pd

class CandidatosSpider(scrapy.Spider):
    name = 'candidatos'
    allowed_domains = ['divulgacandcontas.tse.jus.br']
    start_urls = ['http://divulgacandcontas.tse.jus.br/divulga/rest/v1/eleicao/ordinarias']
    UFs = [
        "AC",
        "AL",
        "AM",
        "AP",
        "BA",
        "CE",
        "DF",
        "ES",
        "GO",
        "MA",
        "MG",
        "MS",
        "MT",
        "PA",
        "PB",
        "PE",
        "PI",
        "PR",
        "RJ",
        "RN",
        "RO",
        "RR",
        "RS",
        "SC",
        "SE",
        "SP",
        "TO",
        'BR'
        ]
    # Apesar de se chamar `UF`, esta  variavel contem `BR` para incluir eleições nacionais
    year = 2018 # Ano de interesse para coleta de dados
    ordinaria = 2

    def parse(self, response):
        """
        Inicializa com a lista das eleições
        """
        for ordin in json.loads(response.text):
            if int(ordin.get('ano')) == self.year:
                # seleciona qual eleição observar com base no ano (self.year)
                self.ordinaria = pd.Series(ordin)
                break
        for uf in self.UFs:
            url = 'http://divulgacandcontas.tse.jus.br/divulga/rest/v1/eleicao/listar/municipios/{}/{}/cargos'.format(
                self.ordinaria.id,
                uf,
            )
            # Gera lista de cargos e seus respectivos ids

            self.logger.info('Running for %s', uf)
            if requests.get(url).json()['cargos']: # possivel fazer por estados
                cargos = pd.DataFrame(requests.get(url).json()['cargos'])[['codigo','nome']]
                candidates_requests = self.parse_cadidates_by_estado(cargos, uf)
                for candidates_request in candidates_requests:
                        yield candidates_request
            else: # necessario fazer por municipios
                url = 'http://divulgacandcontas.tse.jus.br/divulga/rest/v1/eleicao/buscar/{}/{}/municipios'.format(
                    uf,
                    self.ordinaria.id,
                )
                # import ipdb; ipdb.set_trace()
                try:
                    municipios = pd.DataFrame(requests.get(url).json()['municipios'])[['codigo','nome']]
                except KeyError:
                    self.logger.error('%s not in the party?', uf)
                    continue
                for imunicipio, municipio in municipios.iterrows():
                    url = 'http://divulgacandcontas.tse.jus.br/divulga/rest/v1/eleicao/listar/municipios/{}/{}/cargos'.format(
                        self.ordinaria.id,
                        municipio.codigo,
                    )
                    cargos = pd.DataFrame(requests.get(url).json()['cargos'])[['codigo','nome']]
                    candidates_requests = self.parse_cadidates_by_municipio(cargos, municipio)
                    for candidates_request in candidates_requests:
                        yield candidates_request


    def parse_cadidates_by_municipio(self, cargos, municipio):
        for icargos, cargo in cargos.iterrows():
            url = 'http://divulgacandcontas.tse.jus.br/divulga/rest/v1/candidatura/listar/{}/{}/{}/{}/candidatos'.format(
                self.year,
                municipio.codigo,
                self.ordinaria.id,
                cargo.codigo
            )
            # Gera lista de callbacks contendo a lista de candidados, organizado por cargos.
            # Passa as informações do contexto atual como metadados
            yield scrapy.Request(
                url=url,
                callback=self.get_candidate, 
                meta={
                    'uf':municipio.nome,
                    'ufid':municipio.codigo,
                    'ordinaria':self.ordinaria.id,
                    'cargoid':cargo.codigo,
                    'cargo':cargo.nome,
                    'ano': self.year
                    })


    def parse_cadidates_by_estado(self, cargos, uf):
        self.logger.debug('Running for %s', uf)
        for icargos, cargo in cargos.iterrows():
            url = 'http://divulgacandcontas.tse.jus.br/divulga/rest/v1/candidatura/listar/{}/{}/{}/{}/candidatos'.format(
                self.year,
                uf,
                self.ordinaria.id,
                cargo.codigo
            )
            # Gera lista de callbacks contendo a lista de candidados, organizado por cargos.
            # Passa as informações do contexto atual como metadados
            yield scrapy.Request(
                url=url,
                callback=self.get_candidate, 
                meta={
                    'uf':uf,
                    'ufid':uf,
                    'ordinaria':self.ordinaria.id,
                    'cargoid':cargo.codigo,
                    'cargo':cargo.nome,
                    'ano': self.year
                    })

    def get_candidate(self, response):
        """
        Exporta as informações dos candidados eleitos
        """
        candidatos = pd.DataFrame(json.loads(response.text)['candidatos'])

        # Todos os candidatos em situação deferida e com condições diferentes de `não eleito`
        eleitos = candidatos[candidatos.descricaoTotalizacao != 'Não eleito']

        # candidatos em situações de impedimento. Omitido por incoerencia do banco original
        # eleitos = eleitos[eleitos.descricaoSituacao != 'Indeferido']
        # eleitos = eleitos[eleitos.descricaoSituacao != 'Indeferido com recurso']

        # Remove suplentes
        eleitos = eleitos[eleitos.descricaoTotalizacao != 'Suplente']
        for i in  eleitos.to_dict('records'):
            yield i
        self.logger.debug('Candidatos %s[%s]: %d',response.meta['cargo'], response.meta['uf'],len(candidatos))
        self.logger.debug('Eleitos %s[%s]: %d',response.meta['cargo'], response.meta['uf'],len(eleitos))
        for index, can in eleitos.iterrows():
            # monta a URL do candidato na API
            url = 'http://divulgacandcontas.tse.jus.br/divulga/rest/v1/candidatura/buscar/{}/{}/{}/candidato/{}'.format(
                response.meta['ano'],
                response.meta['ufid'],
                response.meta['ordinaria'],
                can.id
            )
            # Salva dados no banco do crawler
            yield requests.get(url).json()

