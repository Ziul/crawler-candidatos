# -*- coding: utf-8 -*-
import scrapy
import pymongo
import requests
import json
import pandas as pd
from ordinarias.settings import MONGO_DB as db_settings

__people_collection_name__ = 'people'

connection = pymongo.MongoClient(
    db_settings.get('MONGODB_SERVER', 'localhost'),
    db_settings.get('MONGODB_PORT', 27017),
    # serverSelectionTimeoutMS=10
    socketTimeoutMS=500
)
db = connection[db_settings.get('MONGODB_DB', 'candidates')][__people_collection_name__]

class WikipediaSpider(scrapy.Spider):
    name = 'wikipedia'
    year = 'wikipedia'
    allowed_domains = ['pt.wikipedia.org', 'google.com']
    start_urls = ['https://pt.wikipedia.org']
    root = 'https://pt.wikipedia.org/w/index.php?sort=relevance&search='

    def parse(self, response):
        """
        Inicializa com a lista já presente no banco
        """
        people = db.find({'cargo':{'$in':[
            'Deputado Federal'
        ]}}, {'nome':1,'cargo':1, 'partido.sigla':1})
        self.logger.warning('%s itens found', people.count())
        for pe in people:
            search = '{}+{}+"{}"+"{}"'.format(
                pe.get('cargo'),
                pe.get('partido').get('sigla'),
                pe.get('nome').get('nomeCompleto'),
                pe.get('nome').get('nomeUrna'),
            )
            # Realiza uma busca na Wikipedia como nome + nome completo + cargo + partido
            yield scrapy.Request(
                url=self.root + search,
                callback=self.crawl_wiki,
                meta={
                    'person':pe,
                    })

    def crawl_wiki(self, response):
        """
        Resultado da busca na Wikipedia
        """
        #  Por ser ordenado por relevância, o primeiro deve ser o correto
        route = response.css('div.mw-search-result-heading a::attr(href)').extract_first()
        pe = response.meta.get('person')
        if not route:
            """
            As vezes a página não contem o nome completo ou o nome de urna.
            Google quase sempre pega quem falta
            """
            g_root = 'https://www.google.com.br/search?q=site:pt.wikipedia.org '
            g_query = '{} "{}"'.format(
                pe.get('partido').get('sigla'),
                pe.get('nome').get('nomeCompleto'),
            )
            #  Tentativa pelo Google (arriscada)
            yield scrapy.Request(
                url=g_root + g_query,
                callback=self.crawl_google,
                meta={
                    'person':pe,
                    'replay': True
                    })
        else:
            yield response.follow(
                url=route,
                callback=self.parse_person,
                meta={
                    'person' : response.meta.get('person'),
                    'replay': response.meta.get('replay', False)
                }
                )

    def crawl_google(self, response):
        pe = response.meta.get('person')
        urls = response.css('a::attr(href)').re('.+https://pt.wikipedia.org/wiki.+')
        twitter = response.css('a::attr(href)').re('.+twitter.+')

        if not urls:
            if not response.meta.get('replay'):
                g_root = 'https://www.google.com.br/search?q='
                g_query = '{} "{}"'.format(
                    pe.get('cargo'),
                    pe.get('nome').get('nomeCompleto'),
                )
                yield scrapy.Request(
                    url=g_root + g_query,
                    callback=self.crawl_google,
                    meta={
                        'person':pe,
                        'replay': True
                        })
                return
            else:
                # import ipdb; ipdb.set_trace()
                self.logger.info(response.url)
                self.logger.warning('Complications with "%s" %s %s - %s',
                    pe.get('nome').get('nomeCompleto'),
                    pe.get('nome').get('nomeUrna'),
                    pe.get('cargo'),
                    pe.get('partido').get('sigla'),
                    )
                return

        yield scrapy.Request(
                url=response.urljoin(urls[0]),
                callback=self.parse_person,
                meta={
                    'person':pe,
                    'google': True,
                    'twitter': twitter
                    })



    def parse_person(self, response):
        """
        Página da pessoa. Salva no banco a URL e os primeiros 512 caracteres da página
        """
        pe = response.meta.get('person')
        # extrai url da imagem no infobox
        urlImage = response.css('table.infobox_v2 img ::attr(src)').extract_first()
        # monta documento e retorna-o
        if response.meta.get('replay'):
            self.logger.debug('found %s - %s!',
                            response.meta.get('person'),
                            response.meta.get('cargo')
                            )
        return {
            'url' : response.url,
            'nomeCompleto' : pe.get('nome').get('nomeCompleto'),
            'cargo' : pe.get('cargo'),
            'resumo' : ''.join(response.css('div#mw-content-text p ::text').extract())[:512],
            'imagemUrl': response.urljoin(urlImage) if urlImage else None,
            'by_google': response.meta.get('google', False),
            'twitter': response.meta.get('twitter', ''),
        }